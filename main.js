import express from "express";
import bodyParser from "body-parser";
import sqlite3 from "sqlite3";
import bcrypt from "bcrypt";
import session from "express-session";

// Verbindung zur Sqlite DB aufbauen
const sqlite = sqlite3.verbose();
// mit Hilfe des DB Objekts interagieren wir mit der Datenbank und ihren Tabellen
const db = new sqlite.Database("./usermanager");
// Table users erstellen
// db.run("CREATE TABLE users ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, email TEXT NOT NULL, password TEXT NOT NULL)")

const port = 3000;
const host = "localhost";

const app = express();
app.set("view engine", "ejs");

app.use(
	session({
		secret: "secretString",
		resave: false,
		saveUninitialized: true,
		cookie: { secure: false }, // für Entwicklungszwecke, setzen Sie es in einer Produktionsumgebung auf true
	})
);

// Ermöglicht das Auslesen von Formulardaten
app.use(bodyParser.urlencoded({ extended: false }));

/**
 * Check if a string is an integer string
 * @param string str
 * @returns
 */
function isIntegerString(str) {
	return /^-?\d+$/.test(str);
}

app.get("/", (req, res) => {
	// Jede Seite muss das Attribut title setzen
	res.render("index", {
		title: "Home",
	});
});

app.get("/users", (req, res) => {
	// Alle user selektieren, wir erhalten ein array mit Objekten
	// Jede Spalte in unserer Tabelle ist ein Key für ein Datum
	db.all("SELECT * FROM users ORDER BY name", (err, rows) => {
		let users = [];
		if (err) {
			console.log("Error: ", err);
		} else {
			// Bei Erfolg speichern wir die Zeilen in users
			users = rows;
		}
		// Users an die view übergeben
		res.render("users", {
			title: "Users",
			users: users,
		});
	});
});

/**
 * Show user edit form
 */
app.get("/users/:id", (req, res) => {
	const id = req.params.id;
	if (!isIntegerString(id)) {
		res.send("Error");
		return;
	}

	// Wenn msg vorhanden ist, merken
	let msg = ""
	if (req.session.msg) {
		msg = req.session.msg
	}

	// User mit id selektieren
	db.get(`SELECT * FROM users WHERE id = ${id}`, (err, row) => {
		if (err) {
			console.log(err);
		} else {
			if (row) {
				// Damit die Message nicht in allen anderen Requests auftaucht,
				// löschen wir sie
				delete req.session.msg

				res.render("user-edit", {
					title: "Edit user",
					data: row,
					// Message an Template weiter geben
					msg: msg
				});
			} else {
				// Wenn user mit id nicht existiert
				res.send("User does not exist");
			}
		}
	});
});

/**
 * Update User data
 */
app.post("/users/:id", (req, res) => {
	const id = req.params.id;
	if (!isIntegerString(id)) {
		res.send("Error");
		return;
	}

	// Post Daten aus dem request auslesen
	const name = req.body.username;
	const email = req.body.email;
	// Simple Validierung
	if (name === "" || email === "") {
		req.formError = "Bitte Name und Email ausfüllen";
		res.send(req.formError);
		return;
	}

	// User mit id selektieren
	// Parametrisierte Query
	db.run(
		"UPDATE users SET name=?, email=? WHERE id=?",
		[name, email, id],
		(err, row) => {
			if (err) {
				req.session.msg = {
					status: "error",
					text: "Fehler beim Speichern."
				}
			} else {
				req.session.msg = {
					status: "success",
					text: "User erfolgreich aktualisiert!"
				}
			}
			res.redirect(`/users/${id}`)
		}
	);
});

/**
 * Delete user. REST API app.delete("/users/:id" ...
 * Browser base app: app.get
 */
app.get("/users/delete/:id", (req, res) => {
	const id = req.params.id;
	// TODO: validieren!
	db.run("DELETE FROM users WHERE id = ?", [id], (err) => {
		if (err) {
			return res.status(500).send("Es trat ein Fehler auf");
		}
		res.redirect("/users");
	});
});

app.get("/register", (req, res) => {
	// Jede Seite muss das Attribut title setzen
	res.render("register", {
		title: "Register User",
	});
});

// register post route, in konsole username & email ausgeben
app.post("/register", async (req, res) => {
	// Daten aus Request body auslesen
	// TODO: validieren der Daten!
	const userName = req.body.username;
	const email = req.body.email;
	const password = req.body.password;
	/*
		Da wir Passwörter NIEMALS plain text in einer DB speichern,
		verschlüsseln wir diese. Wir verwenden bcrypt dazu.
		Beim Speichern wird das PW mit bcrypt verschlüsselt
		und dieser Wert wird in der DB abgelegt.

		Bei der Abfrage des PWs wird dieses nach Absenden ebenfalls
		verschlüsselt und mit dem Wert in der DB verglichen.

		Es handelt sich um eine Ein-Weg Verschlüsselung. Dh. es kann das Orignal
		Passwort nicht mehr ausgelesen werden.
	*/
	const cryptedPW = await bcrypt.hash(password, 10);

	db.run(
		"INSERT INTO users (name, email, password) VALUES(?, ?, ?)",
		[userName, email, cryptedPW],
		(err) => {
			if (err) {
				console.log("Error: ", Error);
			} else {
				console.log("angelegt");
			}
		}
	);
	res.send("angelegt");
	// Jede Seite muss das Attribut title setzen
	// res.render("register", {
	// 	title: "Register User",
	// })
});

app.listen(port, () => {
	console.log(`Server läuft unter http://${host}:${port}`);
});
